// Copyright 2020
// Authors: Radu Nichita, Matei Simtinică

#ifndef BONUS_H_
#define BONUS_H_

#include <bits/stdc++.h>

#include "task.h"

using namespace std;

/*
 * Bonus Task
 * You have to implement 4 methods:
 * read_problem_data         -ss read the problem input and store it however you see fit
 * formulate_oracle_question - transform the current problem instance into a SAT instance and write the oracle input
 * decipher_oracle_answer    - transform the SAT answer back to the current problem's answer
 * write_answer              - write the current problem's answer
 */

class Bonus : public Task {
private:
    int n_families, n_relations;
    int n_variables;
    int n_clauses;
    int max_cost;
    std::vector<std::pair<int, int>> edges;
    std::vector<int> output;

public:
    void solve() override {
        read_problem_data();
        formulate_oracle_question();
        ask_oracle();
        decipher_oracle_answer();
        write_answer();
    }

    void read_problem_data() override {
        ifstream input_file{in_filename};

        input_file >> n_families >> n_relations;
        for (int i = 1; i <= n_relations; i++) {
            int a, b;
            input_file >> a >> b;
            edges.push_back({a, b});
        }

        n_variables = n_families;
        max_cost = std::numeric_limits<int>::max();
    }

    void formulate_oracle_question() {
        stringstream oracle_question;

        // Adaugam clauze "hard" pentru scoaterea cate cel putin un nod de la fiecare muchie
        for (int i = 0; i < n_relations; i++) {
            oracle_question << max_cost << " " << edges[i].first << " " << edges[i].second << " 0\n";
            n_clauses++;
        }

        // Adaugam un cost pe fiecare nod pastrat
        for (int i = 1; i <= n_families; i++) {
            oracle_question << "1 " << -i << " 0\n";
            n_clauses++;
        }

        // Costul va fi minimizat de oracol => minimum vertex cover
        ofstream oracle_out{oracle_in_filename};
        oracle_out << "p wcnf " << n_variables << " " << n_clauses << " " << n_families << "\n";
        oracle_out << oracle_question.rdbuf();
    }

    void decipher_oracle_answer() {
        ifstream sat_output_file{oracle_out_filename};
        string sat_result;

        int variables;
        int validated;

        sat_output_file >> variables >> validated;
        ofstream output_file{out_filename};

        for (int i = 1; i <= n_families; i++) {
            int out;
            sat_output_file >> out;
            if (out > 0) {
                output_file << out << " ";
            }
        }
    }
    void write_answer() override {}
};

#endif // BONUS_H_
