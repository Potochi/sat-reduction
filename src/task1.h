// Copyright 2020
// Authors: Radu Nichita, Matei Simtinică

#ifndef TASK1_H_
#define TASK1_H_

#include "task.h"
#include <bits/stdc++.h>

using namespace std;

/*
 * Task1
 * You have to implement 4 methods:
 * read_problem_data         - read the problem input and store it however you see fit
 * formulate_oracle_question - transform the current problem instance into a SAT instance and write the oracle input
 * decipher_oracle_answer    - transform the SAT answer back to the current problem's answer
 * write_answer              - write the current problem's answer
 */

using EdgeList = vector<pair<int, int>>;

class Task1 : public Task {
private:
    EdgeList edges{};
    int n_clauses = 0;
    int n_families = 0;
    int n_relations = 0;
    int n_spies = 0;
    int n_variables = 0;

public:
    void solve() override {
        read_problem_data();
        formulate_oracle_question();
        ask_oracle();
        decipher_oracle_answer();
        write_answer();
    }

    int get_variable(int family, int spy) {
        return (family - 1) * n_spies + spy;
    }

    void read_problem_data() override {
        ifstream input_data{in_filename};
        input_data >> n_families >> n_relations >> n_spies;
        edges.reserve(n_relations);
        int a, b;
        for (int i = 0; i < n_relations; i++) {
            input_data >> a >> b;
            edges.push_back(std::make_pair(a, b));
        }
        n_variables = n_families * n_spies;
    }

    void formulate_oracle_question() {
        stringstream oracle_question{};

        // Fiecare nod trebuie sa aiba cel putin un spion
        for (int i = 1; i <= n_families; i++) {
            for (int j = 1; j <= n_spies; j++) {
                oracle_question << get_variable(i, j) << " ";
            }
            n_clauses++;
            oracle_question << "0\n";
        }

        // Fiecare nod are cel mult un spion
        for (int i = 1; i <= n_families; i++) {
            for (int j = 1; j <= n_spies; j++) {
                for (int x = j + 1; x <= n_spies; x++) {
                    oracle_question << -get_variable(i, j) << " " << -get_variable(i, x) << " 0\n";
                    n_clauses++;
                }
            }
        }

        // Nodurile adiacente nu pot avea acelasi spion
        for (int i = 0; i < n_relations; i++) {
            for (int j = 1; j <= n_spies; j++) {
                oracle_question << -get_variable(edges[i].first, j) << " " << -get_variable(edges[i].second, j)
                                << " 0\n";
                n_clauses++;
            }
        }

        ofstream oracle_file{oracle_in_filename};
        oracle_file << "p cnf " << n_variables << " " << n_clauses << "\n";
        oracle_file << oracle_question.rdbuf();
    }

    void decipher_oracle_answer() {
        ifstream oracle_out_file{oracle_out_filename};
        ofstream out_file{out_filename};

        string sat_result;
        oracle_out_file >> sat_result;

        if (*sat_result.begin() == 'T') {
            out_file << "True\n";
            int vars;
            oracle_out_file >> vars;
            for (int i = 0; i < vars; i++) {
                int variable;
                oracle_out_file >> variable;
                if (variable > 0) {
                    out_file << ((variable - 1) % n_spies) + 1 << " ";
                }
            }
        } else {
            out_file << "False\n";
        }
    }
    void write_answer() override {}
};

#endif // TASK1_H_
