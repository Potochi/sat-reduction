// Copyright 2020
// Authors: Radu Nichita, Matei Simtinică

#ifndef TASK2_H_
#define TASK2_H_

#include "task.h"
#include <bits/stdc++.h>
using namespace std;

/*
 * Task2
 * You have to implement 4 methods:
 * read_problem_data         - read the problem input and store it however you see fit
 * formulate_oracle_question - transform the current problem instance into a SAT instance and write the oracle input
 * decipher_oracle_answer    - transform the SAT answer back to the current problem's answer
 * write_answer              - write the current problem's answer
 */

using EdgeList = vector<pair<int, int>>;

class Task2 : public Task {
private:
    EdgeList n_edges{};

    int n_families = 0;
    int n_relations = 0;
    int n_family_size = 0;
    int n_clauses = 0;
    int n_variables = 0;

public:
    int get_var_index(int family, int spy) {
        return (family - 1) * n_family_size + spy;
    }

    void solve() override {
        read_problem_data();
        formulate_oracle_question();
        ask_oracle();
        decipher_oracle_answer();
        write_answer();
    }

    void read_problem_data() override {
        ifstream in_file{in_filename};

        in_file >> n_families >> n_relations >> n_family_size;
        n_edges.reserve(n_relations);
        int a, b;
        for (int i = 0; i < n_relations; i++) {
            in_file >> a >> b;
            n_edges.push_back(std::make_pair(a, b));
        }
        n_variables = n_families * n_family_size;
    }

    void formulate_oracle_question() {

        stringstream oracle_question;

        // Avem cel putin un nod pe fiecare pozitie in clica

        for (int i = 1; i <= n_family_size; i++) {
            for (int j = 1; j <= n_families; j++) {
                oracle_question << get_var_index(j, i) << " ";
                n_clauses++;
            }
            oracle_question << "0\n";
        }

        // Dou noduri nu pot fii pe aceeasi pozitie in clica

        for (int i = 1; i <= n_families; i++) {
            for (int j = 1; j <= n_family_size; j++) {
                for (int x = j + 1; x <= n_family_size; x++) {
                    oracle_question << -get_var_index(i, j) << " " << -get_var_index(i, x) << " 0\n";
                    n_clauses++;
                }
            }
        }

        // Noduri neadiacente nu pot fi in clica
        for (int i = 1; i <= n_families; i++) {
            for (int j = i + 1; j <= n_families; j++) {
                auto check_pair = std::make_pair(i, j);
                if (std::find(n_edges.begin(), n_edges.end(), check_pair) == n_edges.end()) {
                    for (int a = 1; a <= n_family_size; a++) {
                        for (int b = 1; b <= n_family_size; b++) {
                            if (a == b)
                                continue;
                            oracle_question << -get_var_index(i, a) << " " << -get_var_index(j, b) << " 0\n";
                            n_clauses++;
                        }
                    }
                }
            }
        }

        ofstream oracle_in_file{oracle_in_filename};

        oracle_in_file << "p cnf " << n_variables << " " << n_clauses << "\n";
        oracle_in_file << oracle_question.rdbuf();
    }

    void decipher_oracle_answer() {
        ifstream oracle_out_file{oracle_out_filename};
        ofstream out_file{out_filename};

        string sat_result;
        oracle_out_file >> sat_result;

        if (*sat_result.begin() == 'T') {
            out_file << "True\n";
            int variables;
            oracle_out_file >> variables;
            for (int i = 0; i < variables; i++) {
                int variable;
                oracle_out_file >> variable;
                if (variable > 0) {
                    out_file << ((variable - 1) / n_family_size) + 1 << " ";
                }
            }
        } else {
            out_file << "False\n";
        }
    }

    void write_answer() override {}
};

#endif // TASK2_H_
