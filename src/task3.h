// Copyright 2020
// Authors: Radu Nichita, Matei Simtinică

#ifndef TASK3_H_
#define TASK3_H_

#include "task.h"
#include "task1.h"
#include "task2.h"

#include <bits/stdc++.h>

using namespace std;

/*
 * Task3
 * This being an optimization problem, the solve method's logic has to work differently.
 * You have to search for the minimum number of arrests by successively querying the oracle.
 * Hint: it might be easier to reduce the current task to a previously solved task
 */

using EdgeList = vector<pair<int, int>>;

class Task3 : public Task {
private:
    int n_families;
    int n_relations;
    int n_clique_size;
    EdgeList n_edges;
    EdgeList n_inverted_edges;
    vector<int> n_output;
    std::string task2_in_filename;
    std::string task2_out_filename;

public:
    void solve() override {
        task2_in_filename = in_filename + "_t2";
        task2_out_filename = out_filename + "_t2";
        Task2 task2_solver = Task2();
        task2_solver.add_files(task2_in_filename, oracle_in_filename, oracle_out_filename, task2_out_filename);
        read_problem_data();

        // Cauta clica maxima din graful inversat
        // ( este echivalenta cu minimum vertex cover in graful initial )
        for (n_clique_size = n_families; n_clique_size > 0; n_clique_size--) {
            reduce_to_task2();
            task2_solver.solve();
            if (extract_answer_from_task2())
                break;
        }
        write_answer();
    }

    void read_problem_data() override {
        ifstream in_file{in_filename};

        in_file >> n_families >> n_relations;
        for (int i = 0; i < n_relations; i++) {
            int a, b;
            in_file >> a >> b;
            n_edges.push_back({a, b});
        }
        // Genereaza graful complementar
        for (int i = 1; i <= n_families; i++) {
            for (int j = 1; j <= n_families; j++) {
                if (i == j) {
                    continue;
                }
                if (find(n_edges.begin(), n_edges.end(), make_pair(i, j)) == n_edges.end()) {
                    n_inverted_edges.push_back({i, j});
                }
            }
        }
    }

    void reduce_to_task2() {
        ofstream task2_in_file{task2_in_filename};

        task2_in_file << n_families << " " << n_inverted_edges.size() << " " << n_clique_size << "\n";

        for (auto edge : n_inverted_edges) {
            task2_in_file << edge.first << " " << edge.second << "\n";
        }
    }

    bool extract_answer_from_task2() {
        ifstream task2_out_file{task2_out_filename};
        std::vector<int> task2_output;

        string sat_result;
        task2_out_file >> sat_result;
        n_output.clear();

        if (*sat_result.begin() == 'T') {
            for(int i = 0 ; i < n_clique_size; i++) {
                int x;
                task2_out_file >> x;
                task2_output.push_back(x);
            }
            for (int i = 1; i <= n_families; i++) {
                // Salvam nodurile din minimum vertex cover
                if (find(task2_output.begin(), task2_output.end(), i) == task2_output.end()) {
                    n_output.push_back(i);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    void write_answer() override {
        ofstream output_file{out_filename};
        for (auto out_variable : n_output) {
            output_file << out_variable << " ";
        }
    }
};

#endif // TASK3_H_
